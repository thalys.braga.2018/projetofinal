<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movimentacoes', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->double('qtd');
            $table->string('lote')->nullable();
            $table->string('tipo');

            $table->foreignId('fabricacoes_id')
            ->nullable()
            ->constrained('fabricacoes')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->foreignId('produtos_id')
            ->constrained('produtos')
            ->onUpdate('cascade')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movimentacoes');
    }
};
