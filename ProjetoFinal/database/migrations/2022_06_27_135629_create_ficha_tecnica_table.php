<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ficha_tecnicas', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->text('observacoes')->nullable();
            $table->float('qtd');

            $table->foreignId('produtos_id')
            ->constrained('produtos')
            ->onUpdate('cascade')
            ->onDelete('cascade');
        });

        Schema::create('ficha_tecnica_produto', function (Blueprint $table) {
            $table->id();
            
            $table->double('quantidade');

            $table->foreignId('ficha_tecnica_id')
            ->constrained('ficha_tecnicas')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->foreignId('produto_id')
            ->constrained('produtos')
            ->onUpdate('cascade')
            ->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ficha_tecnica_produtos');
        Schema::dropIfExists('ficha_tecnica');
    }
};
