<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fabricacoes', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->double('qtd');
            $table->date('data_fabricacao');
            $table->date('data_validade');
            $table->string('lote');
            $table->double('custoFabricacao');


            $table->foreignId('ficha_tecnicas_id')
            ->constrained('ficha_tecnicas')
            ->onUpdate('cascade')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fabricacoes');
    }
};
