<?php

namespace App\Http\Controllers;

use App\Models\FichaTecnica;
use App\Models\Produto;
use Illuminate\Http\Request;

class FichasTecnicas extends Controller
{
    public function index()
    {
        return view('fichaTecnica.index')
        ->with('fichasTecnicas', FichaTecnica::all());
    }

    public function show(Produto $produto, FichaTecnica $fichaTecnica)
    {
        return view('fichaTecnica.show')
        ->with('produto', $produto)
        ->with('fichaTecnica', $fichaTecnica);
    }

    public function create(Produto $produto)
    {
        return view('fichaTecnica.create')
        ->with('produto', $produto);
    }

    public function addMateriaPrima(Produto $produto, FichaTecnica $fichaTecnica)
    {     
        $materiasPrimas = Produto::all()->where('tipo', 'Materia Prima');
        return view('fichaTecnica.addMateriaPrima')
        ->with('produto', $produto)
        ->with('fichaTecnica', $fichaTecnica)
        ->with('materiasPrimas', $materiasPrimas);
    }

    public function store(Request $request)
    {
        $criarFicha = $this->prepararDadosFicha($request->all());  
        $FichaTecnica = FichaTecnica::create($criarFicha);
        return redirect()->route('fichaTecnica.addMateriaPrima', ['produto' => $FichaTecnica->produtos_id, 'fichaTecnica' => $FichaTecnica->id]);
    }

    public function storeMateriaPrima(Request $request, Produto $produto, FichaTecnica $fichaTecnica)
    {
        $materiasPrimas = $request->materiaPrima;
        foreach ($materiasPrimas as $valor) {
            $valor = json_decode($valor);
            $fichaTecnica->produtos()->attach($valor[0], ['quantidade' => $valor[1]]);
        }

        return redirect()->route('produtos.index');
    }

    public function edit(Produto $produto, FichaTecnica $fichaTecnica)
    {
        $materiasPrimas = Produto::all()->where('tipo', 'Materia Prima');

        return view('FichaTecnica.edit')
        ->with('produto', $produto)
        ->with('fichaTecnica', $fichaTecnica)
        ->with('materiasPrimas', $materiasPrimas);
    }

    public function update(Request $request, Produto $produto, FichaTecnica $fichaTecnica)
    {
        //atualizar ficha
        $atualizarFicha = $this->prepararDadosFicha($request->all());
        $fichaTecnica->update($atualizarFicha);

        //remover registros antigos
        foreach ($fichaTecnica->produtos as $prods) {
            $fichaTecnica->produtos()->detach($prods);
        }

        //atualizar materias primas da ficha
        $materiasPrimas = $request->materiaPrima;
        foreach ($materiasPrimas as $materiaPrima) {
            $materiaPrima = json_decode($materiaPrima);
            $fichaTecnica->produtos()->attach($materiaPrima[0], ['quantidade' => $materiaPrima[1]]);
        }

        return redirect()->route('fichaTecnica.show', ['produto' => $produto->id, 'fichaTecnica' => $fichaTecnica->id]);
    }

    public function destroy(Produto $produto, FichaTecnica $fichaTecnica)
    {
        $fichaTecnica->delete();
        return redirect()->route('produtos.show', ['produto' => $produto->id]);
    }

    public function prepararDadosFicha($params)
    {
        return[
            'observacoes' => $params['observacoes'],
            'qtd' => $params['qtdProd'],
            'produtos_id' => $params['idProd']
        ];
    }
}
