<?php

namespace App\Http\Controllers;

use App\Models\Fabricacao;
use App\Models\FichaTecnica;
use App\Models\Movimentacao;
use App\Models\Produto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade\Pdf;


class Fabricacoes extends Controller
{
    public function index()
    {
        return view('fabricacoes.index')
        ->with('fabricacoes', Fabricacao::all()->sortByDesc('data_fabricacao'));
    }

    public function show(Produto $produto, Fabricacao $fabricacao)
    {
        $materiasPrimas = Movimentacao::where('fabricacoes_id', $fabricacao->id)->get();

        return view('fabricacoes.show')
        ->with('fabricacao', $fabricacao)
        ->with('produto', $produto)
        ->with('materiasPrimas', $materiasPrimas);
    }

    public function create(Produto $produto)
    {
        $fichaTecnica = FichaTecnica::all()->firstWhere('produtos_id', $produto->id);

        $fabricacao = DB::table('fabricacoes')->orderByDesc('id')->first();

        if ($fabricacao == null) {
            $lote = 1;
        }else{
            $lote = $fabricacao->lote + 1;
        }

        return view('fabricacoes.create')
        ->with('produto', $produto)
        ->with('lote', $lote)
        ->with('fichaTecnica', $fichaTecnica);
    }

    public function store(Request $request)
    {
        $msgErro = $this->validacoes($request->all());
        if ($msgErro) {
            return redirect()->back()->withInput()->with(
                'mensagem', $msgErro
            );
        }
        
        $custoFabricacao = $this->verificarPercaGanho($request->all());
        $fabricacao = $this->dadosPreparados($request->all(), $custoFabricacao);
        $fabricacaoPronta = Fabricacao::create($fabricacao);

        $materiasPrimas = FichaTecnica::all()->firstWhere('produtos_id', $request->produto);

        foreach ($materiasPrimas->produtos as $chave => $item) {
            $movimentacaoMateriaPrima = $this->dadosPreparadosMovimentacaoMateriaPrima($request->all(), $fabricacaoPronta, $item, $chave);
            Movimentacao::create($movimentacaoMateriaPrima);
        }
        
        $movimentacao = $this->dadosPreparadosMovimentacaoProduto($request->all(), $fabricacaoPronta);
        Movimentacao::create($movimentacao);

        return redirect()->route('produtos.index');
    }

    public function dadosPreparadosMovimentacaoProduto($params)
    {
        return[
            'produtos_id' => $params['produto'],
            'qtd' => $params['qtd'],
            'lote' => $params['lote'],
            'tipo' => 'Entrada'
        ];
    }

    public function dadosPreparadosMovimentacaoMateriaPrima($params, $fabricacao, $materiaPrima, $i)
    {
        return[
            'produtos_id' => $materiaPrima->id,
            'qtd' => ($params["quantidade"][$i]),
            'fabricacoes_id' => $fabricacao->id,
            'lote' => $params['lote'],
            'tipo' => 'Saida'
        ];
    }

    public function dadosPreparados($params, $custoFabricacao)
    {
        return[
            'qtd' => ($params['qtd']),
            'data_fabricacao' => $params['dataFabricacao'],
            'data_validade' => $params['dataValidade'],
            'lote' => $params['lote'],
            'ficha_tecnicas_id' => $params['fichaTecnica'],
            'custoFabricacao' => $custoFabricacao
        ];
    }

    public function validacoes($params)
    {
        $msg = '';
        //validar lote
        $fabricacoes = FichaTecnica::firstWhere('produtos_id', $params['produto'])->fabricacao;
        foreach($fabricacoes as $fabricacao) {
            if ($fabricacao->lote == $params['lote']) {
                if($fabricacao->data_validade != $params['dataValidade'] && $fabricacao->data_fabricacao != $params['dataFabricacao']){
                    $msg = "O lote " . $params['lote'] ." já está em uso com datas de fabricação e validade diferentes. <br>";
                }
            }   
        }

        //validar quantidade
        $fichaTecnica =  FichaTecnica::find($params['fichaTecnica']);

        foreach ($fichaTecnica->produtos as $i => $materiaPrima) {
            $qtdDisponivel = $this->calcularEstoqueEspecifico($materiaPrima);
            if ($qtdDisponivel == null) {
                $qtdDisponivel = 0;
            }

            if ($params["quantidade"][$i] > $qtdDisponivel) {
                $msg .= "$materiaPrima->nome não tem quantidade disponível em estoque. (Disponível: $qtdDisponivel $materiaPrima->unidade_comercial) <br>";
            }
        }
        
        if ($msg == '') {
            return false;    
        }
        
        return $msg;
    }

    public function calcularEstoqueEspecifico(Produto $produto)
    {
        $movimentacoes = Movimentacao::where('produtos_id', $produto->id)->get();

        if ($movimentacoes->count() == 0) {
            return null;
        }

        $qtdEstoque = 0;

        foreach ($movimentacoes as $movimentacao) {
            if($movimentacao->tipo == 'Entrada'){
                $qtdEstoque += $movimentacao->qtd;
            }else{
                $qtdEstoque -= $movimentacao->qtd;
            }
            
        }
        
        return $qtdEstoque;
    }

    public function relatorioGeral()
    {
        $pdf = Pdf::loadView('relatorios.fabricacoesGeral', ['fabricacoes' => Fabricacao::all()]);
        return $pdf->stream();
    }

    public function relatorioEspecifico(Fabricacao $fabricacao)
    {
        $materiasPrimas = Movimentacao::where('fabricacoes_id', $fabricacao->id)->get();
        $pdf = Pdf::loadView('relatorios.fabricacaoEspecifica', ['fabricacao' => $fabricacao, 'materiasPrimas' => $materiasPrimas]);
        return $pdf->stream();
    }

    public function verificarPercaGanho($params)
    {
        $custoFinal = 0;

        //selecionar ficha
        $fichaPadrao = FichaTecnica::find($params['fichaTecnica']);

        //calcular custo
        foreach ($fichaPadrao->produtos as $index => $produto) {
            $diferenca = 0;

            //calcula a quantidade prevista pela ficha
            $qtdOriginal = $produto->pivot->quantidade * $params['qtd'];

            //calcula o custo original gerado previsto pela ficha
            $custoOriginal = $qtdOriginal * $produto->valor_unitario;

            //calcula o custo do produto nesta fabricação
            $custoFabricacao = $params['quantidade'][$index] * $produto->valor_unitario;

            //calcula o custo final
            $custoFinal += ($custoOriginal - $custoFabricacao);
        }

        return $custoFinal;
    }
}
