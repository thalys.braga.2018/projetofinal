<?php

namespace App\Http\Controllers;

use App\Models\Movimentacao;
use App\Models\Produto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade\Pdf;


class estoque extends Controller
{
    public function index()
    {
        $estoques = $this->calcularEstoqueTodos();
        return view('estoque.index')
        ->with('estoques', $estoques);
    }

    

    public function show(Produto $produto)
    {
        $estoque = $this->calcularEstoqueEspecifico($produto);
        $movimentacoes = Movimentacao::where('produtos_id', $produto->id)->get();

        return view('estoque.show')
        ->with('estoque', $estoque)
        ->with('produto', $produto)
        ->with('movimentacoes', $movimentacoes);
    }

    public function calcularEstoqueEspecifico(Produto $produto)
    {
        $movimentacoes = Movimentacao::where('produtos_id', $produto->id)->get();

        if ($movimentacoes->count() == 0) {
            return null;
        }
        $qtdEstoque = 0;
        $array = [];

        foreach ($movimentacoes as $movimentacao) {
            if($movimentacao->tipo == 'Entrada'){
                $qtdEstoque += $movimentacao->qtd;
            }else{
                $qtdEstoque -= $movimentacao->qtd;
            }
            $nome = $movimentacao->produto->nome;
            $id = $movimentacao->produtos_id;

        }
        $array = ['id'=> $id,'nome' => $nome, 'quantidade' => $qtdEstoque];
        return $array;
    }

    public function calcularEstoqueTodos()
    {
        $ultimoProduto = DB::table('produtos')->orderByDesc('id')->first();

        if($ultimoProduto == null){
            return null;
        }

        $array = [];

        for ($i = 1; $i <= $ultimoProduto->id; $i++) {
            $qtdEstoque = 0;
            $movimentacoes = Movimentacao::where('produtos_id', $i)->get();

            if ($movimentacoes->count() == 0) {
                $produto = Produto::firstWhere('id', $i);
                if(!$produto){
                    continue;
                }else{
                    $nome = $produto->nome;
                    $id = $produto->id;
                }
            }

            foreach ($movimentacoes as $item) {
                if($item->tipo == 'Entrada'){
                    $qtdEstoque += $item->qtd;
                }else{
                    $qtdEstoque -= $item->qtd;
                }
                $nome = $item->produto->nome;
                $id = $item->produtos_id;
            }

            $array[] = ['id'=> $id,'nome' => $nome, 'quantidade' => $qtdEstoque];
        }

        return $array;
    }

    public function relatorioGeral()
    {
        $estoque = $this->calcularEstoqueTodos();
        $pdf = Pdf::loadView('relatorios.estoqueGeral', ['estoque' => $estoque]);
        return $pdf->stream();
    }

    public function relatorioEspecifico(Produto $produto)
    {
        $estoque = $this->calcularEstoqueEspecifico($produto);
        $movimentacoes = Movimentacao::where('produtos_id', $produto->id)->get();
        $pdf = Pdf::loadView('relatorios.estoqueEspecifico', ['estoque' => $estoque, 'movimentacoes' => $movimentacoes]);
        return $pdf->stream();
    }
}
