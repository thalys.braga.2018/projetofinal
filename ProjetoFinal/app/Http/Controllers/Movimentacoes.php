<?php

namespace App\Http\Controllers;

use App\Models\Movimentacao;
use App\Models\odel;
use App\Models\Produto;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade\Pdf;

class Movimentacoes extends Controller
{
    public function index()
    {
        return view('movimentacoes.index')
        ->with('movimentacoes', Movimentacao::all()->sortByDesc('lote'));
    }

    public function adicionarMateriaPrima()
    {
        $registros = Produto::all()->where('tipo', 'Materia Prima');
        return view('movimentacoes.adicionarMateriaPrima')
        ->with('registros', $registros);      
    }

    public function store(Request $request)
    {
        $dadosPreparados = $this->dadosPreparados($request->all());  
        $movimentacoes = Movimentacao::create($dadosPreparados);
        return redirect()->route('movimentacoes.index'); 
    }

    public function dadosPreparados($params)
    {
        return[
            'produtos_id' => $params['Produto'],
            'qtd' => $params['qtd'],
            'tipo' => $params['tipo']
        ];
    }

    public function relatorioGeral()
    {
        $movimentacoes = Movimentacao::all()->sortByDesc('lote');
        $pdf = Pdf::loadView('relatorios.movimentacoesGeral', ['movimentacoes' => $movimentacoes]);
        return $pdf->stream();
    }
}
