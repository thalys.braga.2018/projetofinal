<?php

namespace App\Http\Controllers;

use App\Models\FichaTecnica;
use App\Models\Produto;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade\Pdf;

class Produtos extends Controller
{
    public function index()
    {
        return view('produtos.index')
        ->with('produtos', Produto::all()->sortByDesc('tipo'));
    }
    
    public function show(Produto $produto)
    {
        $fichaTecnica = FichaTecnica::all()->firstWhere('produtos_id', $produto->id);
        return view('produtos.show')
        ->with('produto', $produto)
        ->with('fichaTecnica', $fichaTecnica);
    }

    public function create()
    {
        return view('produtos.create');
    }

    public function store(Request $request)
    {
        $dadosPreparados = $this->dadosPreparados($request->all());
        Produto::create($dadosPreparados);
        return redirect()->route('produtos.index');      
    }

    public function destroy(Produto $produto)
    {
        $produto->delete();
        return redirect()->route('produtos.index');
    }

    public function edit(Produto $produto)
    {
        return view('produtos.edit')
        ->with('produto', $produto);
    }

    public function update(Request $request, Produto $produto)
    {
        $dadosPreparados = $this->dadosPreparados($request->all());
        $produto->update($dadosPreparados);
        return redirect()->route('produtos.show', ['produto' => $produto->id]); 
    }

    public function dadosPreparados($params)
    {
        return[
            'nome' => strtoupper($params['nome']),
            'descricao' => $params['descricao'],
            'unidade_comercial' => $params['unidadeComercial'],
            'tipo' => $params['tipo'],
            'valor_unitario' => $params['valorUnitario']
        ];
    }
    
    public function relatorioGeral()
    {
        $pdf = Pdf::loadView('relatorios.produtosGeral', ['produtos' => Produto::all()]);
        return $pdf->stream();
    }

    public function relatorioEspecifico(Produto $produto)
    {
        $fichaTecnica = FichaTecnica::firstWhere('produtos_id', $produto->id);
        $pdf = Pdf::loadView('relatorios.produtoEspecifico', ['produtos' => $produto, 'fichaTecnica' => $fichaTecnica]);
        return $pdf->stream();
    }
   
}
