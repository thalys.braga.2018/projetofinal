<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fabricacao extends Model
{
    protected $table = 'fabricacoes';
    
    protected $guarded = [];

    protected $casts = [
        'data_validade' => 'date',
        'data_fabricacao' => 'date'
    ];

    public function fichaTecnica()
    {
        return $this->hasOne(fichaTecnica::class, 'id', 'ficha_tecnicas_id');
    }

    public function produto()
    {
        return $this->hasOne(produto::class,'id', 'ficha_tecnicas_id');
    }
}
