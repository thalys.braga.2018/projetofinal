<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FichaTecnica extends Model
{
    use HasFactory;

    protected $table = 'ficha_tecnicas';
    
    protected $guarded = [];

    public function produtos()
    {
        return $this->belongsToMany(Produto::class)->withPivot('quantidade');
    }

    public function fabricacao()
    {
        return $this->hasMany(Fabricacao::class, 'ficha_tecnicas_id');
    }

    public function produtoFicha()
    {
        return $this->belongsTo(Produto::class, 'produtos_id');
    }

}
