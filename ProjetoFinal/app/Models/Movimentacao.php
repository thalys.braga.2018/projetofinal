<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Movimentacao extends Model
{
    protected $table = 'movimentacoes';

    protected $guarded = [];

    protected $casts = [
        'created_at' => 'date',
    ];

    public function fabricacao()
    {
        return $this->hasMany(fabricacao::class);
    }

    public function produto()
    {
        return $this->belongsTo(Produto::class, 'produtos_id');
    }
}
