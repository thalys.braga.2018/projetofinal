<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    protected $table = 'produtos';
    
    protected $guarded = [];

    public function fichaTecnica()
    {
        return $this->belongsToMany(FichaTecnica::class);
    }

}
