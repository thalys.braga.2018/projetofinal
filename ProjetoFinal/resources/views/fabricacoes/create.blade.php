@extends('layout')

@section('title', 'Fabricação')

@section('content')

<nav aria-label="breadcrumb">
    <ol class="breadcrumb bg-white">
        <li class="breadcrumb-item"><a href="{{ route('menu') }}">MENU</a></li>
        <li class="breadcrumb-item"><a href="{{ route('produtos.index') }}">PRODUTOS</a></li>
        <li class="breadcrumb-item"><a href="{{ route('produtos.show', ['produto'=>$produto->id]) }}">{{$produto->nome}}</a></li>
      <li class="breadcrumb-item active" aria-current="page">FABRICAR</li>
    </ol>
</nav>

<h3 class="card-title text-center">Fabricar - {{$produto->nome}}</h3>
<div class="p-3">
    @if (session('mensagem'))
        <div class="alert alert-warning">
            {!! session('mensagem') !!}
        </div>        
    @endif
</div>
@if ($fichaTecnica == null)
<div class="p-3">
    <div class="alert alert-warning" role="alert">
        O produto que deseja fabricar não possui uma ficha técnica! Crie a ficha técnica do produto clicando no botão abaixo.
    </div>
    <div>
        <a href= '{{route('fichaTecnica.create', ['produto'=>$produto->id])}}'  title="Ficha técnica" style="align-content: space-between" class="btn btn-info">
            <i class="bi bi-plus"></i>
            Ficha Técnica
        </a>
    </div>
</div>   
@else
    <form action="{{ route('fabricacoes.store', ["produto" => $produto->id]) }}" method="POST">
        @csrf
        
        <input type="hidden" name="fichaTecnica" value="{{$fichaTecnica->id}}">
        <input type="hidden" name="produto" value="{{$produto->id}}">
        <input type="hidden" id="fichaTecnicaQuantidadeId" name="fichaTecnicaQuantidade" value="{{$fichaTecnica->qtd}}">

        <div class="row p-3">
            <div class="col">
                <label for="loteId" class="form-label">Lote</label>
                <div class="row">
                    <div class="col-2">
                        <input 
                        name = "lote" 
                        class="form-control text-center" 
                        type="number" 
                        min="0" 
                        step="1" 
                        id="loteId" 
                        value="{{ old('lote') ?? $lote }}"
                        readonly>
                    </div>
                    <div class="col-3">
                        <input type="button" id="modLote" class="btn btn-info " value="Modificar Lote" onclick="modificarLote()">
                    </div>
                </div>
            </div>
        </div>
        <div class="row p-3">
            <div class="col-4">
                <label for="qtdId" class="form-label">Quantidade</label>
                <input 
                name = "qtd" 
                class="form-control" 
                type="number" min="0" 
                step="0.5" id="qtdId" 
                placeholder="Digite a quantidade"
                value="{{ old('qtd') ?? ''}}"
                >
            </div>
            <div class="col-4">
                <label for="dataFabricacaoId" class="form-label">Data de Fabricação</label>
                <input 
                name = "dataFabricacao" 
                class="form-control" 
                type="date" id="dataFabricacaoId"
                value="{{ old('dataFabricacao') ?? ''}}">      
            </div>
            <div class="col-4">
                <label for="dataValidadeId" class="form-label">Data de Validade</label>
                <input 
                name = "dataValidade" 
                class="form-control" 
                type="date" 
                id="dataValidadeId" 
                value="{{ old('dataValidade') ?? ''}}">
            </div>
        </div>

        <div class="d-flex flex-row-reverse p-3">
            <input id="gerarFabricacao" type="button" class="btn btn-primary" onclick="multiplicarFicha()" value="Gerar fabricação">    
        </div>

        <div class="p-3">
            <table id="tabela" class="table table-light table-striped table-bordered table-hover {{ old('quantidade') ? '' : 'd-none' }}">
                <thead class="thead-dark">
                    <tr class="text-center">
                        <th>Nome</th>
                        <th>Quantidade</th>
                        <th>Unidade Comercial</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($fichaTecnica->produtos as $index => $materiaPrima)
                        <tr>
                            <td>{{$materiaPrima->nome}}</td>
                            <td style="width: 300px">
                                <input data-valor="{{ $materiaPrima->pivot->quantidade }}"
                                       name="quantidade[]"
                                       style="width: 300px"
                                       class="form-control text-center" 
                                       type="number"
                                       min="0"
                                       step="0.5"
                                       id="qtdIngId"
                                       value="{{ old('quantidade.' . $index) ?? $materiaPrima->pivot->quantidade}}">
                            </td>
                            <td class="text-center" style="width: 200px">{{$materiaPrima->unidade_comercial}}</td>
                        </tr>
                    @endforeach
                </tbody>    
            </table>
        </div>
        
        <div id='finalizaFab' class="card-footer text-light bg-dark {{ old('quantidade') ? '' : 'd-none' }}">
            <div class="row mt-3">
                <div class="col-8 text-center">
                    <label class="font-weight-bold" id="msgId">FABRICANDO {{$fichaTecnica->qtd}} {{$produto->unidade_comercial}} DE {{$produto->nome}}</label>
                </div>
                <div class="col-4 d-flex flex-row-reverse">
                    <button type="submit" id="finalizarId" class="btn btn-success">FINALIZAR FABRICAÇÃO</button>
                </div>
            </div>
        </div>
    </form>  
@endif


<script>
    function modificarLote(){
        if($('#loteId').attr('readonly')){
            $('#loteId').attr('readonly', false);
        }else{
            $('#loteId').attr('readonly', true);
        }
    }

    function multiplicarFicha() {
        let lote = $('#loteId').val();
        let dataFabricacao = $('#dataFabricacaoId').val();
        let dataValidade = $('#dataValidadeId').val();
        let quantidade = $('#qtdId').val();
        let msg = '';

        if (lote == '' || dataFabricacao == '' || dataValidade == '' || dataFabricacao > dataValidade || quantidade == '' || quantidade == 0) {
            
            if (lote == '') {
                msg += 'O lote não pode ser nulo. \n';
            }
            if (dataFabricacao == '') {
                msg += 'Selecione a data de fabricação. \n'
            }
            if (dataValidade == '') {
                msg += 'Selecione a data de validade. \n'
            }
            if (dataFabricacao > dataValidade) {
                msg += 'A data de validade não pode ser mais antiga que a data de fabricação. \n'
            }
            if (quantidade == '') {
                msg += 'Digite a quantidade a ser fabricada. \n'
            }
            if (quantidade == 0) {
                msg += 'A quantidade deve ser maior que 0. \n'
            }
            alert(msg);
        } else {
            $('#tabela').removeClass('d-none');
            $('#finalizaFab').removeClass('d-none');

            let inputs = document.querySelectorAll('#qtdIngId');
            let quantidadeInformada = $('#qtdId').val();

            $('#msgId').text("FABRICANDO " + quantidadeInformada + " {{$produto->unidade_comercial}} DE {{$produto->nome}}");

            inputs.forEach(function (inp) {
                let quantidade = $(inp).data('valor');
                
                inp.value = quantidade * (quantidadeInformada / $('#fichaTecnicaQuantidadeId').val());
            });
        } 
    }
</script>

@endsection