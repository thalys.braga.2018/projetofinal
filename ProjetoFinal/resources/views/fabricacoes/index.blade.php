@extends('layout')

@section('title', 'Fabricações')

@section('content')

<nav aria-label="breadcrumb">
    <ol class="breadcrumb bg-white">
      <li class="breadcrumb-item"><a href="{{ route('menu') }}">MENU</a></li>
      <li class="breadcrumb-item active" aria-current="page">FABRICAÇÕES</li>
    </ol>
</nav>

<h3 class="card-title text-center">Fabricações</h3>

@if ($fabricacoes->count() == 0)
    <div class="alert alert-warning p-3" role="alert">
        Não existem fabricações cadastradas no sistema!
    </div>
@else
<div class="text-right p-3">
    <a href="{{ route('relatorio.fabricacoesGeral') }}" target="_blank"  title="Imprimir relatório" style="align-content: space-between" class="btn btn-secondary">
        <i class="bi bi-printer-fill"></i>
    </a>
</div>

<div class="p-3">
    <input class="form-control" type="text" id="busca" placeholder="Digite aqui para pesquisar"/>
</div>

    <div class="p-3">
        <table id="tabelaId" class="table table-light table-striped table-bordered table-hover">
            <thead class="thead-dark text-center">
                <tr>
                    <th>#</th>
                    <th>Nome do Produto</th>
                    <th>Quantidade Fabricada</th>
                    <th>Receita</th>
                    <th>Data de Fabricação</th>
                    <th>Data de Validade</th>
                    <th>Lote</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($fabricacoes as $fabricacao)
                <tr>
                    <td class="text-center font-weight-bold"> {{$loop->iteration}} </td>
                    <td> 
                        <a href="{{ route('fabricacoes.show', ['produto'=>$fabricacao->fichaTecnica->produtoFicha->id, 'fabricacao' => $fabricacao->id]) }}"> 
                            {{$fabricacao->fichaTecnica->produtoFicha->nome}} 
                        </a>
                    </td>
                    <td class="text-right"> {{$fabricacao->qtd}} </td>
                    <td class="text-right"> R$ {{$fabricacao->qtd * $fabricacao->fichaTecnica->produtoFicha->valor_unitario}} </td>
                    <td class="text-center"> {{$fabricacao->data_fabricacao->format('d/m/Y')}} </td>
                    <td class="text-center"> {{$fabricacao->data_validade->format('d/m/Y')}} </td>
                    <td class="text-right"> {{$fabricacao->lote}} </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endif
@push('script')
<script>
    $(function(){
        $("#busca").keyup(function(){
            var texto = $(this).val().toUpperCase();
            console.log(texto);
            $("#tabelaId tbody tr").attr('class', '');
            $("#tabelaId tbody tr").each(function(){
                if($(this).text().indexOf(texto) < 0)
                    $(this).attr('class', 'd-none');
            });
        });
    });
</script>
@endpush
@endsection