@extends('layout')

@section('title', 'Fabricações')

@section('content')

<nav aria-label="breadcrumb">
    <ol class="breadcrumb bg-white">
      <li class="breadcrumb-item"><a href="{{ route('menu') }}">MENU</a></li>
      <li class="breadcrumb-item"><a href="{{ route('produtos.index') }}">PRODUTOS</a></li>
      <li class="breadcrumb-item"><a href="{{ route('produtos.show', ['produto'=>$produto->id]) }}">{{$produto->nome}}</a></li>
      <li class="breadcrumb-item active" aria-current="page">FABRICAÇÕES</li>
    </ol>
</nav>

<h3 class="card-title text-center">FABRICAÇÃO - {{$produto->nome}} </h3>

<div class="text-right p-3">
    <a href="{{ route('relatorio.fabricacaoEspecifica', ['fabricacao' => $fabricacao->id]) }}" target="_blank"  title="Imprimir relatório" style="align-content: space-between" class="btn btn-secondary">
        <i class="bi bi-printer-fill"></i>
    </a>
</div>

<div class="card">
    <div class="card-body">
        <h5 class="card-title">Dados</h5>
        <div class="p-3">
            <p>
                <span class="font-weight-bold"> Lote: </span>
                {{$fabricacao->lote}}
            </p>
            
            <p>
                <span class="font-weight-bold"> Quantidade Fabricada: </span>
                {{$fabricacao->qtd}}
            </p>

            <p>
                <span class="font-weight-bold"> Data de Fabricação: </span>
                {{$fabricacao->data_fabricacao->format('d/m/Y')}}
            </p>

            <p>
                <span class="font-weight-bold"> Data de Validade: </span>
                {{$fabricacao->data_validade->format('d/m/Y')}}
            </p>
            
            <p>
                <span class="font-weight-bold"> Receita: </span>
                R$ {{$fabricacao->qtd * $fabricacao->fichaTecnica->produtoFicha->valor_unitario}}
            </p>

            @if ($fabricacao->custoFabricacao != 0)       
                <p>
                    <span class="font-weight-bold"> Custo da Fabricação: </span>
                    @if ($fabricacao->custoFabricacao < 0)
                        Débito de R$ {{ $fabricacao->custoFabricacao * -1 }}
                    @else
                        Crédito de R$ {{ $fabricacao->custoFabricacao }}    
                    @endif
                </p>
            @endif
        </div>
    </div>
</div><br>

<div class="card">
    <div class="card-body">
        <h5 class="card-title">Matérias Primas Utilizadas</h5>
        <div class="p-3">
            <table id="tabela" class="table table-light table-striped table-bordered table-hover">
                <thead class="thead-dark text-center">
                    <tr>
                        <th>Nome</th>
                        <th>Quantidade</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($materiasPrimas as $prods)
                        <tr>
                            <td>{{$prods->produto->nome}}</td>
                            <td class="text-right">{{$prods->qtd}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection