@extends('layout')

@section('title', 'Fichas Técnicas')

@section('content')

<nav aria-label="breadcrumb">
    <ol class="breadcrumb bg-white">
        <li class="breadcrumb-item"> <a href="{{ route('menu') }}"> MENU </a> </li>
        <li class="breadcrumb-item active" aria-current="page"> FICHAS TÉCNICAS </li>
    </ol>
</nav>

<h3 class="card-title text-center">FICHAS TÉCNICAS</h3>

@if ($fichasTecnicas->count() == 0)
<div class="alert alert-warning" role="alert">
    O sistema não possui fichas técnicas cadastradas! Cadastre novas fichas técnicas.
</div>
@else
<div class="p-3">
    <input class="form-control" type="text" id="busca" placeholder="Digite aqui para pesquisar"/>
</div>

<div class="p-3">
    <table id="tabelaId" class="table table-light table-striped table-bordered table-hover" style="border-radius: 50px;">
        <thead class="thead-dark text-center">
            <tr>
                <th>#</th>
                <th>Nome</th>
                <th>Quantidade</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($fichasTecnicas as $fichaTecnica)
            <tr>
                <td class="text-center font-weight-bold"> {{$loop->iteration}} </td>
                <td><a href="{{ route('fichaTecnica.show', ['produto' => $fichaTecnica->produtos_id, 'fichaTecnica' => $fichaTecnica->id]) }}">{{$fichaTecnica->produtoFicha->nome}}</td>
                <td class="text-right">{{$fichaTecnica->qtd}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endif
@push('script')
<script>
    $(function(){
        $("#busca").keyup(function(){
            var texto = $(this).val().toUpperCase();
            console.log(texto);
            $("#tabelaId tbody tr").attr('class', '');
            $("#tabelaId tbody tr").each(function(){
                if($(this).text().indexOf(texto) < 0)
                    $(this).attr('class', 'd-none');
            });
        });
    });
</script>
@endpush
@endsection