@extends('layout')

@section('title', 'Ficha técnica')

@section('content')

<nav aria-label="breadcrumb">
    <ol class="breadcrumb bg-white">
      <li class="breadcrumb-item"> <a href="{{ route('menu') }}"> MENU </a> </li>
      <li class="breadcrumb-item"> <a href="{{ route('produtos.index') }}"> PRODUTOS </a> </li>
      <li class="breadcrumb-item"> <a href="{{ route('produtos.show', ['produto'=>$produto->id]) }}"> {{$produto->nome}} </a> </li>
      <li class="breadcrumb-item active" aria-current="page"> FICHA TÉCNICA </li>
    </ol>
</nav>

<h3 class="card-title text-center"> FICHA TÉCNICA - {{$produto->nome}} </h3>

<div class="p-3">
    <p>
        <span class="font-weight-bold"> Observações: </span>
        {{$fichaTecnica->observacoes}}
    </p>
    <p>
        <span class="font-weight-bold"> Quantidade de produto relacionado: </span>
        {{$fichaTecnica->qtd}} {{$produto->unidade_comercial}}
    </p>
</div>
<div class="p-3">
    <table class="table table-light table-striped table-bordered table-hover">
        <thead class="thead-dark text-center">
            <tr>
                <th>Nome</th>
                <th>Quantidade</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($fichaTecnica->produtos as $prods)
                <tr>
                    <td>{{$prods->nome}}</td>
                    <td class="text-right">{{$prods->pivot->quantidade}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
    
<div class="p-3 text-center">
    <a href="{{ route('fichaTecnica.edit', ['produto' => $produto->id, 'fichaTecnica' => $fichaTecnica->id]) }}" title="Editar" class="btn btn-primary" style="align-content: space-between">
        <i class="bi bi-pencil"></i>
        Editar
    </a>
    <a title="Excluir" class="btn btn-danger" style="align-content: space-between"  data-toggle="modal" data-target="#modalExcluir">
        <i class="bi bi-trash"></i>
        Excluir
    </a>
</div> 




    <div class="modal fade" id="modalExcluir" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">EXCLUIR PRODUTO</h5>
            </div>
            <div class="modal-body">
            Deseja excluir esse produto? Essa ação é definitiva
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <form action="{{ route('fichaTecnica.destroy', ['produto' => $produto->id, 'fichaTecnica' => $fichaTecnica->id]) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger"> Deletar </button>
                </form>
            </div>
        </div>
        </div>
    </div>
@endsection