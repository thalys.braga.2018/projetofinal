@extends('layout')

@section('title', 'Ficha Técnica')

@section('content')

<nav aria-label="breadcrumb">
    <ol class="breadcrumb bg-white">
        <li class="breadcrumb-item"><a href="{{ route('menu') }}">MENU</a></li>
        <li class="breadcrumb-item"><a href="{{ route('produtos.index') }}">PRODUTOS</a></li>
        <li class="breadcrumb-item"><a href="{{ route('produtos.show', ['produto' => $produto->id]) }}">{{$produto->nome}}</a></li>
        <li class="breadcrumb-item"><a href="{{ route('fichaTecnica.show', ['produto'=>$produto->id, 'fichaTecnica' => $fichaTecnica->id]) }}">FICHA TÉCNICA</a></li>
        <li class="breadcrumb-item active" aria-current="page">EDITAR</li>
    </ol>
</nav>

<h3 class="card-title text-center">Editar Ficha Técnica- {{$produto->nome}}</h3>

<form action="{{ route('fichaTecnica.update', ['produto'=>$produto->id, 'fichaTecnica' => $fichaTecnica->id]) }}" method="post">
	@csrf
    <div class="card p-3">
        <div class="card-body">
            <h5 class="card-title">Dados Principais</h5>
            <div class="p-3">
                <label for="obsInput">Observações</label>
                <textarea class="form-control" id="obsInput" name = "observacoes" cols="10" rows="2" placeholder="Observações...">{{$fichaTecnica->observacoes}}</textarea>
            </div>
            <div class="p-3">
                <label for="qtdProdId">Quantidade do produto</label>
                <input name = "qtdProd" class="form-control" type="number" min="0" step="0.5" id="qtdProdId" placeholder="Digite a quantidade" value="{{$fichaTecnica->qtd}}">
            </div>
        
            <input type="hidden" name="idProd" value="{{$produto->id}}">
        </div>
    </div>
	<br>
    <div class="card p-3">
        <div class="card-body">
            <h5 class="card-title">Matérias Primas</h5>
            <div class="row p-3">
                <div class="col-6">
                    <label for="materiaPrima" class="form-label">Matéria Prima</label>
                        <select id="materiaPrima" class="form-control">
                            <option value=""></option>
                            @foreach ($materiasPrimas as $materiaPrima)
                                <option value="{{ $materiaPrima->nome }}" data-medida = "{{ $materiaPrima->unidade_comercial}}" data-id="{{ $materiaPrima->id }}">{{$materiaPrima->nome}} - {{$materiaPrima->unidade_comercial}}</option>
                            @endforeach
                        </select>
                </div>
                <div class="col-6">
                    <label for="qtdId">Quantidade da matéria prima</label>
                    <input class="form-control" type="number" min="0" step="0.5" id="qtdId" placeholder="Digite a quantidade">
                </div>
            </div>
        
            <div class="row p-3 d-flex flex-row-reverse">
                <div class="col-2">
                    <input type="button" id="cancelar" class="btn btn-danger form-control" onclick="limparCampos()" value="LIMPAR">
                </div>
                <div class="col-2">
                    <input type="button" id="salvar" class="btn btn-primary form-control" onclick="salvarDados()" value="ADICIONAR">
                </div>
            </div>
        
            <div id="divTabela" class="p-3">
                <table id="tabela" class="table table-light table-striped table-bordered table-hover">
                    <thead class="thead-dark text-center">
                        <tr>
                            <th>Matéria Prima</th>
                            <th>Quantidade</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody id="tbody">
                        @foreach ($fichaTecnica->produtos as $produto)
                            <tr>
                                <td> {{$produto->nome}} </td>
                                <td class="text-right"> {{$produto->pivot->quantidade}} </td>
                                <td class="text-center"> 
                                    <a class="btn btn-danger"  onclick="deletar($(this))"><i class="bi bi-trash"></i></a>
                                    <a class="btn btn-primary" onclick="editar($(this))"><i class="bi bi-pencil"></i></a>
                                    <input type="hidden" style="display: none;" name="materiaPrima[]" value="[{{$produto->id}}, {{$produto->pivot->quantidade}}]">
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="text-right">
                    <button type="submit" class="btn btn-success">CONCLUIR CADASTRO</button>
                </div>
            </div>
        </div>
    </div>
</form>

<script>
  function salvarDados(){
	var materiaPrima = $('#materiaPrima').val();
	var quantidade = $('#qtdId').val();

	var idMateriaPrima = $("#materiaPrima option:selected").data("id");
	var unidadeComercial = $("#materiaPrima option:selected").data("medida");
   
    if (!(materiaPrima == '' || quantidade == '')) {
    	var tr = document.createElement("tr");

		var $excluir = $('<a>', {
			'class': 'btn btn-danger',
			'onclick': 'deletar($(this))'
		}).append($('<i class="bi bi-trash"></i>'));

		var $editar = $('<a>', {
			'class': 'btn btn-primary',
			'onclick': 'editar($(this))'
		}).append($('<i class="bi bi-pencil"></i>'));

		var dados = $('<a>').append($(`<input type="hidden" style="display: none;" name="materiaPrima[]" value="[${idMateriaPrima}, ${quantidade}]">`));
		var tdMateriaPrima = $('<td>').append(materiaPrima);
		var tdQuantidade = $('<td>', {
			'class': 'text-right'
		}).append(quantidade);
		var tdAcoes = $('<td>', {
			'class': 'text-center'
		}).append($excluir[0]," ", $editar[0], dados[0]);

		tr.appendChild(tdMateriaPrima[0]); 
		tr.appendChild(tdQuantidade[0]);
		tr.appendChild(tdAcoes[0]);
                   
    	document.getElementById("tabela").appendChild(tr);
   
    	limparCampos();
    }else {
      alert('Todos os campos precisam estar preenchidos !!');
    }
  }

  function editar(botao){
	let tr = botao.parents('tr');
	let ingrediente = tr.find('td').eq(0).text().trim();
	let quantidade  = tr.find('td').eq(1).text().trim();
	
	$('#materiaPrima').val(ingrediente);
	$('#qtdId').val(quantidade);
	deletar(botao);
  }

  function limparCampos(){
    $('#materiaPrima').val('');
	$('#qtdId').val('');
  }
   
  function deletar(botao) {
	botao.closest('tr').remove();
  }
</script>
@endsection