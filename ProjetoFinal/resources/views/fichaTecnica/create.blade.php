@extends('layout')
 
@section('title', 'Ficha técnica')

@section('content')

<nav aria-label="breadcrumb">
    <ol class="breadcrumb bg-white">
      <li class="breadcrumb-item"><a href="{{ route('menu') }}">MENU</a></li>
      <li class="breadcrumb-item"><a href="{{ route('produtos.index',) }}">PRODUTOS</a></li>
      <li class="breadcrumb-item active" aria-current="page">CRIAR FICHA TÉCNICA</li>
    </ol>
</nav>

<h3 class="card-title text-center">Ficha Técnica - {{$produto->nome}}</h3><br>
<form action="{{ route('fichaTecnica.store', ['produto'=>$produto->id]) }}" method="post">
	@csrf
	<div class="p-3">
		<label for="obsInput">Observações</label>
		<textarea class="form-control" id="obsInput" name = "observacoes" cols="10" rows="2" placeholder="Observações..."></textarea>
    </div>
	<div class="p-3">
		<label for="qtdId">Quantidade</label>
		<input name = "qtdProd" class="form-control" type="number" min="0" step="0.5" id="qtdId" placeholder="Digite a quantidade" required>
	</div>

	<div class="text-center p-3">
		<button type="submit" class="btn btn-success">ADICIONAR MATÉRIAS PRIMAS</button>
	</div>
	<input type="hidden" name="idProd" value="{{$produto->id}}">
</form>
@endsection
