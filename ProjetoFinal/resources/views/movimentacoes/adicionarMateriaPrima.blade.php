@extends('layout')

@section('title', 'Movimentação')

@section('content')

<nav aria-label="breadcrumb">
    <ol class="breadcrumb bg-white">
      <li class="breadcrumb-item"><a href="{{ route('menu') }}">MENU</a></li>
      <li class="breadcrumb-item"><a href="{{ route('movimentacoes.index') }}">MOVIMENTAÇÕES</a></li>
      <li class="breadcrumb-item active" aria-current="page">ADICIONAR MATÉRIA PRIMA</li>
    </ol>
</nav>

<div class="p-3">
    <h3 class="card-title text-center"> ADICIONAR MATÉRIA PRIMA </h3>
    <form action="{{ route('movimentacoes.store') }}" method="post">
        @csrf
        <div class="row p-3">
            <div class="col-8">
                <label for="produtoId">Escolha a Matéria Prima</label>
                <select class="form-control" id="produtoId" name="Produto" required>
                    <option value=""></option>
                    @foreach ($registros as $item)
                        <option value="{{$item->id}}">{{$item->nome}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-4">
                <label for="qtdId">Quantidade</label>
                <input name = "qtd" class="form-control" type="number" min="0" step="0.5" id="qtdId" placeholder="Digite a quantidade">
            </div>
        </div>
        <div class="p-3 text-right">
            <input type="hidden" name="tipo" value="Entrada">
    
            <button class="btn btn-success">Registrar</button>
            <a href="{{ route('movimentacoes.index') }}" class="btn btn-danger">Cancelar</a>
        </div>
    </form>
</div>

@endsection