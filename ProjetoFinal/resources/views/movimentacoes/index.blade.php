@extends('layout')

@section('title', 'Fabricações')

@section('content')

<nav aria-label="breadcrumb">
    <ol class="breadcrumb bg-white">
      <li class="breadcrumb-item"><a href="{{ route('menu') }}">MENU</a></li>
      <li class="breadcrumb-item active" aria-current="page">MOVIMENTAÇÕES</li>
    </ol>
</nav>

<h3 class="card-title text-center">Movimentações</h3>

<div class="p-3">
    <a href="{{ route('movimentacoes.adicionarMateriaPrima') }}"  title="Cadastrar movimentação" style="align-content: space-between" class="btn btn-success">
        <i class="bi bi-plus"></i>
        ADICIONAR MATÉRIAS PRIMAS
    </a>
</div>

@if ($movimentacoes->count() == 0)
<div class="p-3">
    <div class="alert alert-warning" role="alert">
        Não existem movimentações cadastradas no sistema!
    </div>
</div>
@else
<div class="text-right p-3">
    <a href="{{ route('relatorio.movimentacoesGeral') }}" target="_blank"  title="Imprimir relatório" style="align-content: space-between" class="btn btn-secondary">
        <i class="bi bi-printer-fill"></i>
    </a>
</div>

<div class="p-3">
    <input class="form-control" type="text" id="busca" placeholder="Digite aqui para pesquisar"/>
</div>

<div class="p-3">
    <table id="tabelaId" class="table table-light table-striped table-bordered table-hover">
        <thead class="thead-dark text-center">
            <tr>
                <th>#</th>
                <th>Nome do Produto</th>
                <th>Quantidade Movimentada</th>
                <th>Lote</th>
                <th>tipo</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($movimentacoes as $linha)
            <tr>
                <td class="text-center font-weight-bold"> {{$loop->iteration}} </td>
                <td> {{$linha->produto->nome}} </td> 
                <td class="text-right"> {{$linha->qtd}} </td> 
                <td class="text-right"> {{$linha->lote}} </td>
                <td class="text-center"> {{$linha->tipo}} </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div> 
@endif

@push('script')
<script>
    $(function(){
        $("#busca").keyup(function(){
            var texto = $(this).val().toUpperCase();
            console.log(texto);
            $("#tabelaId tbody tr").attr('class', '');
            $("#tabelaId tbody tr").each(function(){
                if($(this).text().indexOf(texto) < 0)
                    $(this).attr('class', 'd-none');
            });
        });
    });
</script>
@endpush
@endsection