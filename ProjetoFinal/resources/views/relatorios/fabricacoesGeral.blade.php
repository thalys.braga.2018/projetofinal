@extends('layoutRelatorios')

@section('title', 'Relatório geral das Fabricações')

@section('content')
    <h1 class="text-center">FABRICAÇÕES</h1>
    <div class="p-3">
        <table id="tabelaId" class="table table-light table-striped table-bordered table-hover">
            <thead class="thead-dark text-center">
                <tr>
                    <th>#</th>
                    <th>Nome do Produto</th>
                    <th>Quantidade Fabricada</th>
                    <th>Data de Fabricação</th>
                    <th>Data de Validade</th>
                    <th>Lote</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($fabricacoes as $fabricacao)
                <tr>
                    <td class="text-center font-weight-bold"> {{$loop->iteration}} </td>
                    <td> {{$fabricacao->fichaTecnica->produtoFicha->nome}} </td>
                    <td class="text-right"> {{$fabricacao->qtd}} </td>
                    <td class="text-center"> {{$fabricacao->data_fabricacao->format('d/m/Y')}} </td>
                    <td class="text-center"> {{$fabricacao->data_validade->format('d/m/Y')}} </td>
                    <td class="text-right"> {{$fabricacao->lote}} </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection