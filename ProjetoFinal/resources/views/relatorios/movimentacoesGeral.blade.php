@extends('layoutRelatorios')

@section('title', 'Relatório geral das Fabricações')

@section('content')
    <h1 class="text-center">MOVIMENTAÇÕES</h1>
    <div class="p-3">
        <table id="tabelaId" class="table table-light table-striped table-bordered table-hover">
            <thead class="thead-dark text-center">
                <tr>
                    <th>#</th>
                    <th>Nome do Produto</th>
                    <th>Quantidade Movimentada</th>
                    <th>Lote</th>
                    <th>tipo</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($movimentacoes as $linha)
                <tr>
                    <td class="text-center font-weight-bold"> {{$loop->iteration}} </td>
                    <td> {{$linha->produto->nome}} </td> 
                    <td class="text-right"> {{$linha->qtd}} </td> 
                    <td class="text-right"> {{$linha->lote}} </td>
                    <td class="text-center"> {{$linha->tipo}} </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div> 
@endsection