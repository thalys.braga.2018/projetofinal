@extends('layoutRelatorios')

@section('title', 'Relatório específico do produto')

@section('content')
<h1 class="text-center"> {{$produtos->nome}} </h1> <br><br>

<h5> DADOS DO PRODUTO </h5>
<div class="p-3">
    <p>
        <span class="font-weight-bold"> Descrição: </span>
        {{$produtos->descricao}}
    </p>
    <p>
        <span class="font-weight-bold"> Valor Unitário: </span>
        R$ {{$produtos->valor_unitario}}
    </p>
    <p>
        <span class="font-weight-bold"> Unidade Comercial: </span>
        {{$produtos->unidade_comercial}}
    </p>
    <p>
        <span class="font-weight-bold"> Tipo: </span>
        {{$produtos->tipo}}
    </p>
</div>

@if ($fichaTecnica != null)
    <h5> FICHA TÉCNICA </h5>
    <div class="p-3">
        <p>
            <span class="font-weight-bold"> Observações: </span>
            {{$fichaTecnica->observacoes}}
        </p>
        <p>
            <span class="font-weight-bold"> Quantidade de produto relacionado: </span>
            {{$fichaTecnica->qtd}} {{$produtos->unidade_comercial}}
        </p>
    </div>
    <div class="p-3">
        <table class="table table-light table-striped table-bordered table-hover">
            <thead class="thead-dark text-center">
                <tr>
                    <th>Nome</th>
                    <th>Quantidade</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($fichaTecnica->produtos as $materiaPrima)
                    <tr>
                        <td>{{$materiaPrima->nome}}</td>
                        <td class="text-right">{{$materiaPrima->pivot->quantidade}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@else
    @if ($produtos->tipo != 'Materia Prima')
        <h5 class="text-center"> Este produto não possui ficha técnica </h5>
    @endif
@endif
@endsection