@extends('layoutRelatorios')

@section('title', 'Relatório geral do estoque')

@section('content')
    <h1 class="text-center">ESTOQUE</h1>
    <div class="p-3">
        <table id="tabelaId" class="table table-light table-striped table-bordered table-hover">
            <thead class="thead-dark text-center">
                <tr>
                    <th>#</th>
                    <th>Nome</th>
                    <th>Quantidade</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($estoque as $produto)
                <tr>
                    <td class="text-center font-weight-bold"> {{$loop->iteration}} </td>
                    <td class="text-left"> {{$produto['nome']}} </td>
                    <td class="text-right"> {{$produto['quantidade']}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection