@extends('layoutRelatorios')

@section('title', 'Relatório geral dos produtos')

@section('content')
    <h1 class="text-center">PRODUTOS</h1>
    <div class="p-3">
        <table id="tabelaId" class="table table-light table-striped table-bordered table-hover" style="border-radius: 25px;">
            <thead class="thead-dark text-center">
                <tr>
                    <th>#</th>
                    <th>Nome</th>
                    <th>Valor Unitário</th>
                    <th>Unidade Comercial</th>
                    <th>Tipo</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($produtos as $item)
                <tr>
                    <td class="text-center font-weight-bold"> {{$loop->iteration}} </td>
                    <td>{{$item->nome}}</td>
                    <td class="text-right">R$ {{$item->valor_unitario}}</td>
                    <td class="text-center"> {{$item->unidade_comercial}} </td>
                    <td class="text-center">{{$item->tipo}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection






