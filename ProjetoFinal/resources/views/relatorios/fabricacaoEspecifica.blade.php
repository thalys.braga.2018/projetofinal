@extends('layoutRelatorios')

@section('title', 'Relatório específico da Fabricação')

@section('content')
    <h3 class="card-title text-center">FABRICAÇÃO - {{$fabricacao->produto->nome}} </h3>


    <h5>Dados</h5>
    <div class="p-3">
        <p>
            <span class="font-weight-bold"> Lote: </span>
            {{$fabricacao->lote}}
        </p>
        <p>
            <span class="font-weight-bold"> Quantidade Fabricada: </span>
            {{$fabricacao->qtd}}
        </p>
        <p>
            <span class="font-weight-bold"> Data de Fabricação: </span>
            {{$fabricacao->data_fabricacao->format('d/m/Y')}}
        </p>
        <p>
            <span class="font-weight-bold"> Data de Validade: </span>
            {{$fabricacao->data_validade->format('d/m/Y')}}
        </p>

        @if ($fabricacao->custoFabricacao != 0)       
            <p>
                <span class="font-weight-bold"> Custo da Fabricação: </span>
                @if ($fabricacao->custoFabricacao < 0)
                    Débito de R$ {{ $fabricacao->custoFabricacao * -1 }}
                @else
                    Crédito de R$ {{ $fabricacao->custoFabricacao }}  
                @endif  
            </p>
        @endif
        
    </div>


    <h5>Matérias Primas Utilizadas</h5>
    <div class="p-3">
        <table id="tabela" class="table table-light table-striped table-bordered table-hover">
            <thead class="thead-dark text-center">
                <tr>
                    <th>Nome</th>
                    <th>Quantidade</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($materiasPrimas as $item)
                    <tr>
                        <td>{{$item->produto->nome}}</td>
                        <td class="text-right">{{$item->qtd}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection

