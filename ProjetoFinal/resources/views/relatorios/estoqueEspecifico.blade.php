@extends('layoutRelatorios')

@section('title', 'Relatório especifico do estoque')

@section('content')
    <h1 class="text-center">ESTOQUE - {{$estoque['nome']}}</h1>

    <div class="p-3">
        <p>
            <span class="font-weight-bold"> Quantidade em Estoque: </span>
            {{$estoque['quantidade']}}
        </p>
    </div>

    <div class="p-3">
        <table class="table table-light table-striped table-bordered table-hover">
            <thead class="thead-dark text-center">
                <tr>
                    <th>Data</th>
                    <th>Quantidade</th>
                    <th>Tipo</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($movimentacoes as $movimentacao)
                    <tr>
                        <td class="text-center"> {{$movimentacao->created_at->format('d/m/Y')}} </td>
                        <td class="text-right"> {{$movimentacao->qtd}} </td>
                        <td class="text-center"> {{$movimentacao->tipo}} </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection