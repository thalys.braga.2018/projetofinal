@extends('layout')

@section('title', 'Estoque dos Produtos')

@section('content')

<nav aria-label="breadcrumb">
    <ol class="breadcrumb bg-white">
      <li class="breadcrumb-item"><a href="{{ route('menu') }}">MENU</a></li>
      <li class="breadcrumb-item active" aria-current="page">Estoque</li>
    </ol>
</nav>

<h3 class="card-title text-center">ESTOQUE</h3>

@if ($estoques == null)
<div class="p-3">
    <a href="{{ route('produtos.create') }}"  title="Cadastrar Produto" style="align-content: space-between" class="btn btn-success">
        <i class="bi bi-plus"></i>
        NOVO
    </a>
</div>

<div class="alert alert-warning p-3" role="alert">
    Você não possui produtos! Cadastre novos produtos clicando no botão acima.
</div>

@else
<div class="text-right p-3">
    <a href="{{ route('relatorio.estoqueGeral') }}" target="_blank"  title="Imprimir relatório" style="align-content: space-between" class="btn btn-secondary">
        <i class="bi bi-printer-fill"></i>
    </a>
</div>

<div class="p-3">
    <input class="form-control" type="text" id="busca" placeholder="Digite aqui para pesquisar"/>
</div>

<div class="p-3">
    <table id="tabelaId" class="table table-light table-striped table-bordered table-hover">
        <thead class="thead-dark text-center">
            <tr>
                <th>#</th>
                <th>Nome</th>
                <th>Quantidade</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($estoques as $estoque)
            <tr>
                <td class="text-center font-weight-bold"> {{$loop->iteration}} </td>
                <td class="text-left"> <a href="{{ route('estoque.show', ['produto'=>$estoque['id']]) }}"> {{$estoque['nome']}} </a> </td>
                <td class="text-right"> {{$estoque['quantidade']}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif

@push('script')
<script>
    $(function(){
        $("#busca").keyup(function(){
            var texto = $(this).val().toUpperCase();
            console.log(texto);
            $("#tabelaId tbody tr").attr('class', '');
            $("#tabelaId tbody tr").each(function(){
                if($(this).text().indexOf(texto) < 0)
                    $(this).attr('class', 'd-none');
            });
        });
    });
</script>
@endpush
@endsection