@extends('layout')

@section('title', 'Estoque dos Produtos')

@section('content')

<nav aria-label="breadcrumb">
    <ol class="breadcrumb bg-white">
      <li class="breadcrumb-item"><a href="{{ route('menu') }}">MENU</a></li>
      <li class="breadcrumb-item"><a href="{{ route('estoque.show', ['produto'=>$produto->id]) }}">ESTOQUE</a></li>
      <li class="breadcrumb-item active" aria-current="page">{{$produto->nome}}</li>
    </ol>
</nav>

@if ($estoque == null)
    <div class="p-3">
        <div class="alert alert-warning" role="alert">
            Não existem registros de movimentações e estoque para esse produto!
        </div>
    </div> 
@else
<h3 class="card-title text-center">ESTOQUE - {{$produto->nome}} </h3>

<div class="text-right p-3">
    <a href="{{ route('relatorio.estoqueEspecifico', ['produto' => $produto]) }}" target="_blank"  title="Imprimir relatório" style="align-content: space-between" class="btn btn-secondary">
        <i class="bi bi-printer-fill"></i>
    </a>
</div>

<div class="p-3">
    <p>
        <span class="font-weight-bold"> Quantidade em Estoque: </span>
        {{$estoque['quantidade']}}
    </p>
</div>

<div class="p-3">
    <table class="table table-light table-striped table-bordered table-hover">
        <thead class="thead-dark text-center">
            <tr>
                <th>#</th>
                <th>Data</th>
                <th>Quantidade</th>
                <th>Tipo</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($movimentacoes as $movimentacao)
                <tr>
                    <td class="text-center"> {{$loop->iteration}} </td>
                    <td class="text-center"> {{$movimentacao->created_at->format('d/m/Y')}} </td>
                    <td class="text-right"> {{$movimentacao->qtd}} </td>
                    <td class="text-center"> {{$movimentacao->tipo}} </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif
@endsection