@extends('layout')

@section('title', 'Produtos')

@section('content')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-white">
            <li class="breadcrumb-item"><a href="{{ route('menu') }}">MENU</a></li>
            <li class="breadcrumb-item"><a href="{{ route('produtos.index') }}">PRODUTOS</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{$produto->nome}}</li>
        </ol>
    </nav>

    <h3 class="card-title text-center"> {{$produto->nome}} </h3>

    <div class="text-right p-3">
        <a href="{{ route('relatorio.produtoEspecifico', ['produto' => $produto->id]) }}" target="_blank"  title="Imprimir relatório" style="align-content: space-between" class="btn btn-secondary">
            <i class="bi bi-printer-fill"></i>
        </a>
    </div>

    <div class="p-3">
        <p>
            <span class="font-weight-bold"> Descrição: </span>
            {{$produto->descricao}}
        </p>

        <p>
            <span class="font-weight-bold"> Valor Unitário: </span>
            R$ {{$produto->valor_unitario}}
        </p>

        <p>
            <span class="font-weight-bold"> Unidade Comercial: </span>
            {{$produto->unidade_comercial}}
        </p>

        <p>
            <span class="font-weight-bold"> Tipo: </span>
            {{$produto->tipo}}
        </p>
    </div>

    <div class="text-center">
        <a href="{{ route('produtos.edit', ['produto'=>$produto->id]) }}" style="align-content: space-between"  title="Editar dados" class="btn btn-info">
            <i class="bi bi-pencil-square"></i>
            Editar
        </a>

        <a title="Excluir" class="btn btn-danger" style="align-content: space-between"  data-toggle="modal" data-target="#modalExcluir">
            <i class="bi bi-trash"></i>
            Excluir
        </a>

        <a href="{{ route('estoque.show', ['produto'=>$produto->id]) }}"  title="Visualizar estoque" style="align-content: space-between" class="btn btn-dark">
            <i class="bi bi-archive"></i>
            Estoque
        </a>
        
        @if ($produto->tipo != 'Materia Prima')
            <a href="{{ route('fabricacoes.create', ['produto' => $produto->id]) }}"  title="Fabricar Produto" style="align-content: space-between" class="btn btn-info">
                <i class="bi bi-plus-square"></i>
                Fabricar
            </a>

            @if ($fichaTecnica == null)
            <a href= '{{route('fichaTecnica.create', ['produto'=>$produto->id])}}'  title="Ficha técnica" style="align-content: space-between" class="btn btn-info">
                <i class="bi bi-clipboard-data"></i>
                Ficha Técnica
            </a>

            @else
            <a href= '{{ route('fichaTecnica.show', ['produto' => $produto->id, 'fichaTecnica' => $fichaTecnica->id]) }}'  title="Ficha técnica" style="align-content: space-between" class="btn btn-info">
                <i class="bi bi-clipboard-data"></i>
                Ficha Técnica
            </a> 

            @endif
        @endif
        
        <div class="modal fade" id="modalExcluir" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">EXCLUIR PRODUTO</h5>
                    </div>

                    <div class="modal-body">
                        Deseja excluir esse produto? Essa ação é definitiva
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>

                        <form action="{{ route('produtos.destroy', ['produto' => $produto->id]) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">Deletar</button>
                        </form>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection