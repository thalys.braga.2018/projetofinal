@extends('layout')

@section('title', 'Produtos')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-white">
            <li class="breadcrumb-item"><a href="{{ route('menu') }}">MENU</a></li>
            <li class="breadcrumb-item"><a href="{{ route('produtos.index') }}">PRODUTOS</a></li>
            <li class="breadcrumb-item active" aria-current="page">NOVO</li>
        </ol>
    </nav>

    <h3 class="card-title text-center">NOVO PRODUTO</h3>

    <form method="POST" action="{{ route('produtos.store') }}" enctype="multipart/form-data">
        @csrf
        <div class="p-3">
            <label for="nomeInput">Nome</label>
            <input class="form-control" name="nome" id="nomeInput" type="text" placeholder="Digite o nome do produto" required>
        </div>

        <div class="p-3">
            <label for="descricaoInput">Descrição</label>
            <textarea class="form-control" name="descricao" id="descricaoInput" cols="10" rows="2" placeholder="Descreva o produto"></textarea>
        </div>

        <div class="row p-3">
            <div class="col-4">
                <label for="valorUnitarioId">Valor Unitário</label>
                <input class="form-control" type="number" min="0" step="0.05" name="valorUnitario" id="valorUnitarioId" placeholder="Digite o valor Unitário" required>
            </div>

            <div class="col-4">
                <label for="unidadeComercialId">Unidade Comercial</label>
                <select class="form-control" name="unidadeComercial" id="unidadeComercialId" required>
                    <option value="">                         </option>
                    <option value="UN">   Unidade (UN)        </option>
                    <option value="CX">   Caixa (CX)          </option>
                    <option value="M">    Metro (M)           </option>
                    <option value="M2">   Metro quadrado (M2) </option>
                    <option value="M3">   Metro cúbico (M3)   </option>
                    <option value="KG">   Quilo (KG)          </option>
                    <option value="LT">   Litro (LT)          </option>
                    <option value="TON">  Tonelada (TON)      </option>
                    <option value="GAR">  Garrafa (GAR)       </option>
                    <option value="SC">   Saca 60Kg (SC)      </option>
                    <option value="SC50"> Saca 50Kg (SC50)    </option>
                    <option value="DZ">   Dúzia (DZ)          </option>
                    <option value="BD">   Balde (BD)          </option>
                    <option value="FD">   Fardo (FD)          </option>
                    <option value="PCT">  Pacote (PCT)        </option>
                    <option value="HL">   Hectolitro (HL)     </option>
                    <option value="ST">   Estéreo (ST)        </option>
                </select>
            </div>

            <div class="col-4">
                <label for="tipoId">Tipo</label>
                <select class="form-control" id="tipoId" name="tipo" required>
                    <option value=""></option>
                    <option value="Produto Final">Produto Final</option>
                    <option value="Materia Prima">Matéria Prima</option>
                </select>
            </div>
        </div>

        <div class="p-3 text-right">
            <button class="btn btn-success" title="Salvar" type="submit">Salvar</button>
            <a class="btn btn-danger" title="Cancelar" href="{{ route('produtos.index') }}">Cancelar</a>
        </div>
    </form>
@endsection