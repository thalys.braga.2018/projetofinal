@extends('layout')

@section('title', 'Produtos')

@section('content')

<nav aria-label="breadcrumb">
    <ol class="breadcrumb bg-white">
      <li class="breadcrumb-item"><a href="{{ route('menu') }}">MENU</a></li>
      <li class="breadcrumb-item active" aria-current="page">PRODUTOS</li>
    </ol>
</nav>

<h3 class="card-title text-center">PRODUTOS</h3>

<div class="row p-3">
    <div class="col-8">
        <a href="{{ route('produtos.create') }}" title="Cadastrar Produto" style="align-content: space-between" class="btn btn-success">
            <i class="bi bi-plus"></i>
            NOVO
        </a>
    </div>
    <div class="col-4 text-right">
        <a href="{{ route('relatorio.produtosGeral') }}" target="_blank"  title="Imprimir relatório" style="align-content: space-between" class="btn btn-secondary">
            <i class="bi bi-printer-fill"></i>
        </a>
    </div>
</div>

@if ($produtos->count() == 0)
<div class="alert alert-warning" role="alert">
    O sistema não possui produtos cadastrados! Cadastre novos produtos clicando no botão acima.
</div>
@else
<div class="p-3">
    <input class="form-control" type="text" id="busca" placeholder="Digite aqui para pesquisar"/>
</div>

<div class="p-3">
    <table id="tabelaId" class="table table-light table-striped table-bordered table-hover" style="border-radius: 25px;">
        <thead class="thead-dark text-center">
            <tr>
                <th>#</th>
                <th>Nome</th>
                <th>Valor Unitário</th>
                <th>Tipo</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($produtos as $produto)
            <tr>
                <td class="text-center font-weight-bold"> {{$loop->iteration}} </td>
                <td><a href="{{ route('produtos.show', ['produto' => $produto->id]) }}">{{$produto->nome}}</td>
                <td class="text-right">R$ {{$produto->valor_unitario}}</td>
                <td class="text-center">{{$produto->tipo}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endif
@push('script')
<script>
    $(function(){
        $("#busca").keyup(function(){
            var filtro = $(this).val().toUpperCase();
            console.log(filtro);
            $("#tabelaId tbody tr").attr('class', '');
            $("#tabelaId tbody tr").each(function(){
                if($(this).text().indexOf(filtro) < 0)
                    $(this).attr('class', 'd-none');
            });
        });
    });
</script>
@endpush
@endsection

