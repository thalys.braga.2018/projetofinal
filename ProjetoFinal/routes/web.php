<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

use App\Models\Fabricacao;
use App\Models\FichaTecnica;
use App\Models\Movimentacao;
use App\Models\Produto;
use Illuminate\Routing\RouteGroup;

$produtos = App\Http\Controllers\Produtos::class;
$fabricacoes = App\Http\Controllers\Fabricacoes::class;
$movimentacoes = App\Http\Controllers\Movimentacoes::class;
$fichaTecnica = App\Http\Controllers\FichasTecnicas::class;
$estoque = App\Http\Controllers\Estoque::class;
$relatorios = App\Http\Controllers\Relatorios::class;

/**
 * MENU
 */

Route::get('/', function () {
    return view('menu');
})->name('menu');

/**
 * PRODUTOS
 */
Route::get('/produtos', [$produtos,'index'])->name('produtos.index');
Route::get('/produtos/create', [$produtos,'create'])->name('produtos.create');
Route::get('/produtos/{produto}', [$produtos,'show'])->name('produtos.show');
Route::post('/produtos/store', [$produtos,'store'])->name('produtos.store');
Route::delete('/produtos/destroy/{produto}', [$produtos,'destroy'])->name('produtos.destroy');
Route::get('/produtos/edit/{produto}', [$produtos,'edit'])->name('produtos.edit');
Route::post('/produtos/update/{produto}', [$produtos,'update'])->name('produtos.update');
Route::get('/relatorio/produtos', [$produtos, 'relatorioGeral'])->name('relatorio.produtosGeral');
Route::get('/relatorio/produtos/{produto}', [$produtos, 'relatorioEspecifico'])->name('relatorio.produtoEspecifico');

/**
 * FICHA TÉCNICA
 */

Route::get('/fichaTecnica', [$fichaTecnica,'index'])->name('fichaTecnica.index');
Route::get('/produtos/{produto}/fichaTecnica/create', [$fichaTecnica,'create'])->name('fichaTecnica.create');
Route::get('/produtos/{produto}/fichaTecnica/{fichaTecnica}/addMateriaPrima', [$fichaTecnica,'addMateriaPrima'])->name('fichaTecnica.addMateriaPrima');
Route::post('/produtos/{produto}/fichaTecnica/{fichaTecnica}/store', [$fichaTecnica,'storeMateriaPrima'])->name('fichaTecnica.storeMateriaPrima');
Route::get('/produtos/{produto}/fichaTecnica/{fichaTecnica}', [$fichaTecnica,'show'])->name('fichaTecnica.show');
Route::post('/produtos/{produto}/fichaTecnica/store', [$fichaTecnica,'store'])->name('fichaTecnica.store');
Route::delete('/produtos/{produto}/fichaTecnica/destroy/{fichaTecnica}', [$fichaTecnica,'destroy'])->name('fichaTecnica.destroy');
Route::get('/produtos/{produto}/fichaTecnica/edit/{fichaTecnica}', [$fichaTecnica,'edit'])->name('fichaTecnica.edit');
Route::post('/produtos/{produto}/fichaTecnica/update/{fichaTecnica}', [$fichaTecnica,'update'])->name('fichaTecnica.update');

/**
 * FABRICAÇÕES
 */

Route::get('/fabricacoes', [$fabricacoes,'index'])->name('fabricacoes.index');
Route::get('/produtos/{produto}/fabricacao/create', [$fabricacoes,'create'])->name('fabricacoes.create');
Route::post('/produtos/{produto}/fabricacao/store', [$fabricacoes,'store'])->name('fabricacoes.store');
Route::get('/produtos/{produto}/fabricacao/{fabricacao}', [$fabricacoes,'show'])->name('fabricacoes.show');
Route::get('/relatorio/fabricacoes', [$fabricacoes, 'relatorioGeral'])->name('relatorio.fabricacoesGeral');
Route::get('/relatorio/fabricacoes/{fabricacao}', [$fabricacoes, 'relatorioEspecifico'])->name('relatorio.fabricacaoEspecifica');

/**
 * MOVIMENTAÇÕES
 */

Route::get('/movimentacoes', [$movimentacoes,'index'])->name('movimentacoes.index');
Route::get('/movimentacoes/adicionarMateriaPrima', [$movimentacoes,'adicionarMateriaPrima'])->name('movimentacoes.adicionarMateriaPrima');
Route::post('/movimentacoes/store', [$movimentacoes,'store'])->name('movimentacoes.store');
Route::get('/relatorio/movimentacoes', [$movimentacoes, 'relatorioGeral'])->name('relatorio.movimentacoesGeral');

/**
 * ESTOQUE
 */

Route::get('/estoque', [$estoque,'index'])->name('estoque.index');
Route::get('/estoque/produto/{produto}', [$estoque,'show'])->name('estoque.show');
Route::get('relatorio/estoque', [$estoque, 'relatorioGeral'])->name('relatorio.estoqueGeral');
Route::get('/relatorio/estoque/{produto}', [$estoque, 'relatorioEspecifico'])->name('relatorio.estoqueEspecifico');


